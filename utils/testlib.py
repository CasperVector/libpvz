#!/usr/bin/python3

def pad(xws):
	m = max(w for _, w in xws)
	return [" " * (m - w) + x for x, w in xws]

def flat_struct(ist, atom, arr, ret = lambda m, _: m):
	return (ret(m, member) for member in ist for m in ((
		arr(member, idx) for idx in range(member[-1])
	) if member[-1] else [atom(member)]))

def gen_hfs(ist):
	ret, ket = [], None
	for mem in flat_struct(
		ist, lambda m: (False, m[0], m[0]), lambda m, i: (
			i + 1 == m[-1], "%s[%d]" % (m[0], i),
			("" if i else m[0] + "[") + "%d" % i
		), lambda m, member: \
			[m[0]] + pad([(m[2], len(m[2])), member[1 : 3]]) + [m[1]]
	):
		sep = ["", ""] if ket == None else (
			(["]", " "] if mem[1].startswith(" ") else ["] ", "  "])
			if ket else [" ", " "]
		)
		ret.append([sep[0] + mem[1], sep[1] + mem[2], mem[3]])
		ket = mem[0]

	if ket:
		ret[-1][0] += "]"
	ret = list(zip(*ret))
	return "".join(ret[0]), "".join(ret[1]), ret[2]

# vim:noet:ts=4:sw=4
