#!/usr/bin/python3

import os
import re
import subprocess
import sys
from buildlib import *

progFile, bldFile, cfgFile, testFile = \
	("_cffi", "kinds"), ("_cffi", "build.py"), \
	("pyvz", "_cfg.py"), ("examples", "pyvz_test.py")
opaque = ["mem_cookie", "xcb_connection_t"]

declFmt = \
r"""extern void free (void *addr);""" + "\n"

defFmt = \
r'''#include "libpvz_base.h"
#include "libpvz_mem.h"''' + "\n"

bldFmt = \
r'''#!/usr/bin/python3

import cffi
from os import path
ffi = cffi.FFI()
src = path.join(path.dirname(__file__), "..", "src")

ffi.set_source("_pvz", """
%s""", libraries = ["pvz"], include_dirs = [src],
	library_dirs = [path.join(src, ".libs")],
	extra_compile_args = ["-std=c99", "-Wall"]
)

ffi.cdef("""
%s""")

if __name__ == "__main__":
	ffi.compile()

# vim:noet:ts=4:sw=4''' + "\n"

cfgFmt = \
r"""nodes = [
%s
]

structs = [
%s
]

# vim:noet:ts=4:sw=4""" + "\n"

testHead = \
r"""#!/usr/bin/python3

import pyvz
import pyvz_utils
import sys
from pyvz_testlib import *

nodes = [
%s
]

structs = [
%s
]""" + "\n"

testFoot = r"""def test_node(mt, node):
	sys.stdout.write("%s = %s\n" % (
		node[0], node[1] % getattr(mt, node[0]).get()
	))

def test_struct(ms, ist):
	st = getattr(ms, ist[0])
	ret = st.update()
	sys.stdout.write("\n# ms.%s.update() = %s\n" % (ist[0], str(ret)))
	if ret:
		hdr, fmt = gen_hfs(ist[1])[0 : 2]
		hdr, fmt = "# idx %s\n" % hdr, " %%4d %s\n" % fmt
		sys.stdout.write(hdr)
		[sys.stdout.write(fmt % tuple([idx] + list(flat_struct(
			ist[1], lambda m: getattr(s, m[0]), lambda m, i: getattr(s, m[0])[i]
		)))) for idx, s in enumerate(st.struct())]
	return ret

def main():
	with pyvz.Memory(pyvz_utils.get_pid()) as mem, \
		pyvz.MemTree(mem) as mt, pyvz.Structs(mt) as ms:
		sys.stdout.write("# mt.root.update() = %s\n" % str(mt.root.update()))
		[test_node(mt, node) for node in nodes]
		all(test_struct(ms, ist) for ist in structs)

if __name__ == "__main__":
	main()

# vim:noet:ts=4:sw=4""" + "\n"

def indent(s, lvl):
	return "\n".join("\t" * lvl + l if l else "" for l in s.split('\n'))

def func_defs(s):
	ret = re.search(
		r'^extern "C" \{\n#endif\n+(.*\n+)#ifdef __cplusplus\n\}',
		s, flags = re.DOTALL | re.MULTILINE
	).group(1)
	for pat in [r"^#define (.*\\\n)*.*\n"] + [
		r"^typedef struct %s %s;\n" % (st, st) for st in opaque
	]:
		ret = re.sub(pat, "", ret, flags = re.MULTILINE)
	return ret

def get_nodes(src):
	nodes, tree = [], get_memtree(src)
	def visit(node):
		if node["name"]:
			nodes.append((node["name"], node["kind"], node["fmt"][1]))
		[visit(sub) for sub in node["sub"]]
	visit(tree)
	return nodes

def main(src, dest):
	df = lambda *args: os.path.join(dest, *args)
	my_arr = lambda ss: indent(",\n".join(ss), 1)
	nodes, structs = get_nodes(src), get_structs(src)

	open(df(*bldFile), "w").write(bldFmt % (
		defFmt, "".join([
			"typedef ... %s;\n" % kind for kind in opaque
		] + [
			subprocess.Popen(
				df(*progFile), stdout = subprocess.PIPE
			).communicate()[0].decode("UTF-8"),
			"\n"
		] + [
			func_defs(open(os.path.join(src, f)).read())
			for f in [bHFile, mHFile]
		] + [declFmt])
	))

	open(df(*cfgFile), "w").write(cfgFmt % (
		my_arr('("%s", "%s")' % (node[0], node[1]) for node in nodes),
		my_arr('"%s"' % st["name"] for st in structs)
	))

	open(df(*testFile), "w").write(testHead % (
		my_arr('("%s", "%s")' % (node[0], node[2]) for node in nodes),
		my_arr('("%s", [\n%s\n])' % (st["name"], my_arr(
			str(m) for m in gen_ist(st["members"], py = True)
		)) for st in structs)
	) + "\n" + testFoot)

if __name__ == "__main__":
	main(*sys.argv[1:])

# vim:noet:ts=4:sw=4
