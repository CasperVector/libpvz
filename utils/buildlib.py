#!/usr/bin/python3

import math
import re
import os
import sys

bHFile = "libpvz_base.h"
bmFile = "basemem.h"
bxFile = "basex11.h"

mHFile = "libpvz_mem.h"
mbFile = "memtree.h"

mtFile = "memtree.txt"
mtHFile = "memtree_gen.h"
mtCFile = "memtree_gen.c"
mttCFile = "memtree_test.c"

msFile = "memstruct.h"
msiFile = "memstruct_int.h"
msHFile = "memstruct_gen.h"
msCFile = "memstruct_gen.c"
mstCFile = "memstruct_test.c"

def indent(s, lvl):
	return "\n".join("\t" * lvl + l if l else "" for l in s.split('\n'))

def kind_info(kind, hex, const):
	m = re.match(r"(u?)int([0-9]+)_t", kind)
	if not m:
		fmt = "%% 2.3%se" if const else "%%.1%sf"
		var, size = {"float": ("", 4), "double": ("l", 8)}[kind]
		return (fmt % var, fmt % ""), 10, size
	m = m.groups()

	var, size = "x" if hex else ("u" if m[0] else "d"), int(m[1])
	if hex:
		wid, fmt = 2 + int(size / 4), "0x%0" + "%d" % int(size / 4)
	else:
		wid = (0 if m[0] else 1) + \
			int(math.ceil((size if m[0] else size - 1) * math.log(2, 10)))
		fmt = "%" + (("" if m[0] else " ") + "%d" % wid if const else "")
	return (fmt + '" PRI%s%s "' % (var, m[1]), fmt + var), wid, int(size / 8)

def get_memtree(src):
	trees = []
	for line in open(os.path.join(src, mtFile)):
		l = re.sub(r"[ \t]*#.*$", "", line.rstrip())
		if not l:
			continue
		tabs, offset, name, kind = re.match(
			r"^(\t*)([x0-9A-Fa-f]+)[ \t]*" # `name' optional, so `*' not `+'.
			r"([_A-Za-z0-9]*)[: \t]*([,_A-Za-z0-9]*)$",
			l
		).groups()
		lvl, nodes = tabs.count("\t"), trees
		for i in range(0, lvl):
			nodes = nodes[-1]["sub"]
		nodes.append({"name": name, "kind": kind, "offset": offset, "sub": []})

	assert len(trees) == 1
	trees[0]["name"] = "root"
	def visit(node):
		ptr = node["sub"] or node["name"].endswith("Addr")
		hex = ptr or node["kind"].startswith("uint")
		node["kind0"] = node["kind"]
		if not node["kind"]:
			node["kind"] = "uint32_t" if (ptr or any(
				node["name"].endswith(s) for s in ["Cnt", "Max"]
			)) else "int32_t"
		node["fmt"] = kind_info(node["kind"], hex, const = False)[0]
		[visit(sub) for sub in node["sub"]]
	visit(trees[0])
	return trees[0]

def get_structs(src):
	structs = []
	for name, defs in re.findall(
		r"struct ([^ ]+) \{([^{}]+)\}", open(os.path.join(src, msFile)).read()
	):
		members, prev = [], "0x0"
		name, defs = re.sub(r"^pvz_", "", name), re.findall(
			r"\t+([^ \t]+)[ \t]+([^][ \t]+)(?:\[([x0-9A-Fa-f]+)\])?;"
			r"(?:[ \t]*// ([x0-9A-Fa-f]+)\.)?", defs
		)
		for kind, member, num, cur in defs:
			ki = kind_info(kind, member == "addr", const = True)
			if cur:
				if int(cur, 0) - int(prev, 0) != \
					ki[2] * (int(num, 0) if num else 1):
					sys.stderr.write("`%s': sizeof (%s) != (%s - %s).\n" % (
						name, member, cur, prev
					))
				prev = cur
			if not re.match(r"pad[0-9]*", member):
				members.append((member,) + ki[0 : 2] + (
					int(num, 0) if num else None,
				))
		structs.append({"name": name, "members": members, "size": prev})
	return structs

def gen_ist(members, py):
	return ((member[0], member[1][py]) + member[2:] for member in members)

# vim:noet:ts=4:sw=4
