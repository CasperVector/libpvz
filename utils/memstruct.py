#!/usr/bin/python3

import os
import re
import sys
from buildlib import *
from testlib import *

msHHead = \
r"""// struct_*() requires pms to be a pointer to either NULL or an array
// constructed by a previous call to corresponding struct_*().  struct_*()
// returns number of failed elements, or < 0 on serious failure; if struct_*()
// returns < 0, *pms will be free()d (if non-NULL) and set to NULL.  Otherwise,
// *pms will be an array that ends with an extra tail element with .addr = 0,
// and the array should be free()ed or recycled with corresponding struct_*()
// after use; *len will be set to number of read elements, whether successful or
// failed, but excluding the tail.""" + "\n"

msHBody = \
r"""extern int struct_%(name)s
	(pid_t pid, struct mem_tree const *mt, struct pvz_%(name)s **pms, size_t *len);""" + "\n"

msCHead = \
r"""#include <stdlib.h>
%s
#define EXPAND_CAT(A, B) A##B
#define MY_ASSERTER(ID) EXPAND_CAT(_asserter_, ID)
#define MY_ASSERT(EXPR) typedef char MY_ASSERTER(__LINE__) [(EXPR) ? 1 : -1];

static size_t less (size_t a, size_t b) {
	return a < b ? a : b;
}

static size_t more (size_t a, size_t b) {
	return a > b ? a : b;
}

// Each realloc() requests space for (cnt + 1) elements, with the tail reserved
// for the returned tail and as scratch space for proc_get() when the array is
// about to be full.  When resizing array inside the reading loop, requested
// space is at least one element longer than before.  Also note that realloc()
// is not forbidden to change address even when shrinking memory.""" % "".join(
	'#include "%s"\n' % s for s in [bmFile, mbFile, mtHFile, msFile, msHFile, msiFile]
) + "\n"

msCBody = lambda dic: \
(r"""MY_ASSERT (sizeof (struct pvz_%%(name)s) == %%(size)s + sizeof (uint32_t))
int struct_%%(name)s (
	pid_t pid, struct mem_tree const *mt, struct pvz_%%(name)s **pms, size_t *len
) {
	int ret = 0;  // Might be uninitialised on return if max is 0.
	mem_cookie *cookie;
	%(max)s
	uint32_t addr = %%(name)s_addr;
	struct pvz_%%(name)s *ms = *pms, *cur;

	if (proc_open (&cookie, pid, PROC_READ)) { ret = -1; goto open_err; }
	if (!(cur = ms = realloc (*pms, (%(cnt)s + 1) * sizeof (struct pvz_%%(name)s))))
		{ ret = -1; goto alloc_err; }
	for (size_t bc = 0; bc < max; ++bc) {%(loop)s	}
	cur->addr = 0;
	*pms = %(pms)s;
	*len = %(len)s;

	alloc_err:
	if (proc_close (&cookie) && ret != -1) ret = -2;
	open_err:
	if (ret < 0) { if (*pms) free (*pms); *pms = NULL; }
	return ret;
}""" % ({
	"max": r"size_t idx = 0, max = %(name)s_max, cnt = %(name)s_cnt;",
	"pms": r"""(cur = realloc (
		ms, (idx + 1) * sizeof (struct pvz_%(name)s)
	)) ? cur : ms""", "len": r"idx", "cnt": r"cnt", "loop" : r"""
		ret += proc_get (cookie, addr, %(size)s, cur) != 0;
		if (%(name)s_active) {
			cur++->addr = addr;
			if (++idx > cnt) {
				cnt = more (cnt * 1.618, cnt + 1);
				cur = realloc (ms, (cnt + 1) * sizeof (struct pvz_%(name)s));
				if (cur) ms = cur;
				else { *pms = ms; ret = -1; goto alloc_err; }
				cur += idx;
			}
		}
		addr += %(size)s;""" + "\n"
} if dic["cnt"] else {
	"max": r"size_t max = %(name)s_max;", "pms": r"ms",
	"len": r"max", "cnt": r"max", "loop": r"""
		ret += proc_get (cookie, addr, %(size)s, cur) != 0;
		cur++->addr = addr;
		addr += %(size)s;""" + "\n"
})) % dic + "\n"

testCBody = \
r"""struct pvz_%(name)s *%(name)sData = NULL;
printf (
	"\n# struct_%(name)s (pid, mt, &%(name)sData, &len) = %%d\n",
	struct_%(name)s (pid, &mt, &%(name)sData, &len)
);
if (!%(name)sData) { ret = 3; goto post_mt_err; } else idx = 0;
printf ("# idx %(hdr)s\n");
for (idx = 0; idx < len; ++idx) printf (
	" %%4d %(fmt)s\n",
	idx, %(arg)s
);
free (%(name)sData);""" + "\n"

def do_output(dest, structs):
	intT = open(os.path.join(src, msiFile)).read()
	with open(os.path.join(dest, msHFile), "w") as msH, \
		open(os.path.join(dest, msCFile), "w") as msC, \
		open(os.path.join(dest, mstCFile), "w") as testC:
		msH.write(msHHead + "\n")
		msC.write(msCHead + "\n")
		for st in structs:
			hfs = list(gen_hfs(gen_ist(st["members"], py = False)))
			st.update({
				"cnt": "#define %s_active" % st["name"] in intT,
				"hdr": hfs[0], "fmt": hfs[1], "arg": ", ".join(
					"%sData[idx].%s" % (st["name"], hf) for hf in hfs[2]
				)
			})
			msH.write(msHBody % st)
			msC.write(msCBody(st) + "\n")
			testC.write(testCBody % st + "\n")
		msH.write("\n")

if __name__ == '__main__':
	src, dest = sys.argv[1:]
	do_output(dest, get_structs(src))

# vim:noet:ts=4:sw=4
