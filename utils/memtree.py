#!/usr/bin/python3

import os
import re
import sys
from buildlib import *

mtHHead = \
r"""struct mem_tree {""" + "\n"

mtHFoot = \
r"""};

// Returns 0 on success, 1 on failure; will set mt->root to NULL on failure.
extern int tree_init (struct mem_tree *mt);
// Will set mt->root to NULL.
extern void tree_fin (struct mem_tree *mt);""" + "\n"

mtHBody = \
r"""	struct mem_node *%(name)s;  // %(kind)s.""" + "\n"

mtCHead = \
r"""#include <stdlib.h>
#include <string.h>
%s
static void _tree_fin (struct mem_node *tree) {
	// During tree_init(), for non-root nodes, .deg is initialised as 0 due to
	// zeroing of allocated memory in calloc(), and is changed iff calloc()
	// succeeds.  For the root node, calloc() failure immediately leads to
	// `return 1'.  Therefore, the tree is always consistent, including on
	// memory allocation failure.
	for (int i = 0; i < tree->deg; ++i) _tree_fin (tree->ptr + i);
	if (tree->deg > 0) {
		free (tree->ptr);
		tree->ptr = NULL;
		tree->deg = 0;
	}
}

int tree_init (struct mem_tree *mt) {
	struct mem_node *root = calloc (1, sizeof (struct mem_node)), *node = root;  // Tree not forest.
	if (!root) goto root_err; else --node;""" % "".join(
	'#include "%s"\n' % s for s in [bmFile, mbFile, mtHFile]
) + "\n"

mtCFoot = \
r"""	root->addr = root->offset;  // Otherwise root never gets the correct address.
	return 0;
	branch_err:
	_tree_fin (root);
	mt->root = NULL;
	root_err:
	return 1;
}

void tree_fin (struct mem_tree *mt) {
	_tree_fin (mt->root);
	free (mt->root);
	mt->root = NULL;
}""" + "\n"

mtCBody = \
r"""(%(member)s++node)->offset = %(offset)s;""" + "\n"

mtCSize = \
r"""node->deg = -(int) sizeof (%(kind)s);""" + "\n"

mtCNest = \
r"""{
	struct mem_node *tree = node, *node = tree->ptr = calloc (%(deg)d, sizeof (struct mem_node));
	if (node) { tree->deg = %(deg)d; --node; } else goto branch_err;""" + "\n"

mtCDenest = \
r"""}""" + "\n"

testCBody = \
r"""printf (%(fmt)s, node_get (%(kind)s, mt.%(name)s));""" + "\n"

def do_output(dest, tree):
	with open(os.path.join(dest, mtHFile), "w") as mtH, \
		open(os.path.join(dest, mtCFile), "w") as mtC, \
		open(os.path.join(dest, mttCFile), "w") as testC:

		def fmt_mem_tree(idx):
			node, lvl = tree, len(idx)
			for i in idx:
				node = node["sub"][i]

			node.update({
				"deg": len(node["sub"]),
				"member": node["name"] and ("mt->%s = " % node["name"])
			})

			mtC.write(indent(mtCBody % node, lvl + 1))
			if node["kind0"]:
				mtC.write(indent(mtCSize % node, lvl + 1))
			if node["name"]:
				node["fmt"] = r'"%s = %s\n"' % (node["name"], node["fmt"][0])
				mtH.write(mtHBody % node)
				testC.write(testCBody % node)
			if node["sub"]:
				mtC.write(indent(mtCNest % node, lvl + 1))
				for i in range(0, len(node["sub"])):
					fmt_mem_tree(idx + [i])
				mtC.write(indent(mtCDenest % node, lvl + 1))

		mtH.write(mtHHead)
		mtC.write(mtCHead)
		fmt_mem_tree([])
		mtH.write(mtHFoot)
		mtC.write(mtCFoot)
		[obj.write("\n") for obj in [mtH, mtC, testC]]

if __name__ == "__main__":
	src, dest = sys.argv[1:]
	do_output(dest, get_memtree(src))

# vim:noet:ts=4:sw=4
