#!/usr/bin/python3

import os
import re
import sys
from buildlib import *

fmt = r"""#ifndef %(guard)s
#define %(guard)s

%(inc)s
#ifdef __cplusplus
extern "C" {
#endif

%(txt)s#ifdef __cplusplus
}
#endif
#endif

// vim:noet:ts=%(ind)s:sw=%(ind)s""" + "\n"

def proc_input(src, inputs):
	inc, txt = set(), []

	def my_key(s):
		l = s.split("/")
		return len(l), l

	def repl(s):
		inc.update(re.findall(r"#include <[^<>]+>\n", s.group(0)))
		return ""

	for name in inputs:
		with open(os.path.join(src, name)) as f:
			txt.append("/* From `%s'. */\n\n" % name + re.sub(
				r"(#include <[^<>]+>\n)+\n*", repl,
				re.sub(r"\n// vim:[^\n]+", "", f.read())
			))

	return sorted(inc, key = my_key), txt

def do_output(output, ind, inc, txt):
	open(output, "w").write(fmt % {
		"guard": os.path.basename(output).upper().replace(".", "_") + "_",
		"inc": "".join(sorted(inc)), "txt": "".join(txt), "ind": ind
	})

if __name__ == '__main__':
	src, output = sys.argv[1:]
	inputs, ind = {
		bHFile: ([bmFile, bxFile], "4"),
		mHFile: ([mbFile, mtHFile, msFile, msHFile], "8")
	}[os.path.basename(output)]
	do_output(output, ind, *proc_input(src, inputs))

# vim:noet:ts=4:sw=4
