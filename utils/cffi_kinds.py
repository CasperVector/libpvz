#!/usr/bin/python3

import os
import sys

kinds = [
	"size_t", "pid_t", "xcb_window_t",
	"xcb_keycode_t", "xcb_button_t"
], ["sys/types.h", "xcb/xproto.h"]

[sys.stdout.write("#include <%s>\n" % h) for h in kinds[1]]
sys.stdout.write("#include <stdio.h>\n\nint main (void) {\n")
[sys.stdout.write(
	'\t%(decl)s %(var)s = -1;\n'
	'\tprintf ("typedef %%sint%%lu_t %(kind)s;\\n", '
	'%(var)s < 0 ? "" : "u", 8 * sizeof (%(kind)s));\n' % {
		"kind": kind, "decl": kind, "var": "x%d" % idx
	}
) for idx, kind in enumerate(kinds[0])]
sys.stdout.write("\treturn 0;\n}\n\n")

# vim:noet:ts=4:sw=4
