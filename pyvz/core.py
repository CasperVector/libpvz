__all__ = [
	"READ", "WRITE", "PvzError",
	"Window", "Memory", "MemTree", "Structs"
]

import sys
from . import _cfg
from ._pvz import lib, ffi

READ, WRITE = lib.PROC_READ, lib.PROC_WRITE

class PvzError(Exception):
	pass

def _int(x):
	return int(x) if type(x) == float else x

def _err_val(desc, deny):
	if deny:
		raise PvzError(desc)

def _warn_val(name, want, val):
	if val == want:
		return True
	else:
		sys.stderr.write("pvz.core: %s = %d\n" % (name, val))
		return False

def _gc(cls):
	def init(*args, **kwargs):
		class Wrap(object):
			def __enter__(self):
				self.obj = cls(*args, **kwargs)
				return self.obj
			def __exit__(self, exc_type, exc_value, traceback):
				self.obj._fin()
		return Wrap()
	return init

@_gc
class Window(object):
	def __init__(self, win):
		self.win, self._conn = -1, lib.x11_connect()
		_err_val("error with x11_connect()", not self._conn)
		_err_val("error with win_attach()", lib.win_attach(self._conn, win))
		self.win = win
		self._key, self._press = ffi.new("xcb_keycode_t *"), ffi.new("int *")

	def _fin(self):
		if self.win != -1:
			_warn_val("win_detach()", 0, lib.win_detach(self._conn, self.win))
		if self._conn:
			lib.x11_disconnect(self._conn)

	def get_key(self):
		ret = lib.x11_get_key(self._conn, self._key, self._press)
		if ret == 0:
			return self._key[0], self._press[0]
		else:
			_warn_val("x11_get_key()", 1, ret)
			return None, None

	def put_key(self, key, press = True):
		return _warn_val("win_put_key()", 0, lib.win_put_key(
			self._conn, self.win, key, press
		))

	def put_btn(self, x, y, btn = 1, press = True):
		x, y = _int(x), _int(y)
		return _warn_val("win_put_btn()", 0, lib.win_put_btn(
			self._conn, self.win, x, y, btn, press
		))

@_gc
class Memory(object):
	def __init__(self, pid):
		self.pid, self._cookie = pid, ffi.new("mem_cookie **")

	def _fin(self):
		pass

	def open(self, mode):
		return _warn_val("proc_open()", 0, lib.proc_open(
			self._cookie, self.pid, mode
		))

	def close(self):
		return _warn_val("proc_close()", 0, lib.proc_close(self._cookie))

	def get(self, addr, size):
		buf = ffi.new("char []", size)
		if _warn_val("proc_get()", 0, lib.proc_get(
			self._cookie[0], addr, size, buf
		)):
			return ffi.buffer(buf)

	def put(self, addr, buf):
		if not isinstance(buf, ffi.CData):
			buf = ffi.from_buffer(buf)
		return _warn_val("proc_put()", 0, lib.proc_put(
			self._cookie[0], addr, ffi.sizeof(buf), buf
		))

class _MemNode(object):
	def __init__(self, kind, node, mem):
		kind = "%s *" % kind
		self.node, self._mem, self._buf = node, mem, ffi.new(kind)
		self._ptr = ffi.cast(kind, ffi.addressof(self.node, "val"))

	def update(self, recurse = True):
		return _warn_val("tree_update()", 0, lib.tree_update(
			self._mem.pid, self.node, recurse
		))

	def get(self):
		return self._ptr[0]

	def put(self, val):
		self._buf[0] = val
		return self._mem.put(self.node.addr, self._buf)

@_gc
class MemTree(object):
	def __init__(self, mem):
		self._mem, self._tree = mem, ffi.new("struct mem_tree *")
		_err_val("error with tree_init()", lib.tree_init(self._tree))
		for name, kind in _cfg.nodes:
			setattr(
				self, name,
				_MemNode(kind, getattr(self._tree, name), mem)
			)

	def _fin(self):
		if self._tree.root:
			lib.tree_fin(self._tree)

class _Struct(object):
	def __init__(self, name, tree):
		self._name, self._tree, self._len = name, tree, ffi.new("size_t *")
		self._pms = ffi.new("struct pvz_%s **" % name)
		self._struct = getattr(lib, "struct_%s" % name)

	def _fin(self):
		if self._pms[0]:
			lib.free(self._pms[0])

	def struct(self):
		if self._pms[0]:
			return ffi.cast(
				"struct pvz_%s [%d]" % (self._name, self._len[0]), self._pms[0]
			)

	def update(self):
		ret = _warn_val("struct_%s()" % self._name, 0, self._struct(
			self._tree._mem.pid, self._tree._tree, self._pms, self._len
		))
		return ret

@_gc
class Structs(object):
	def __init__(self, tree):
		[setattr(self, name, _Struct(name, tree)) for name in _cfg.structs]

	def _fin(self):
		[getattr(self, name)._fin() for name in _cfg.structs]

# vim:noet:ts=4:sw=4
