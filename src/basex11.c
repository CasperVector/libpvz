#include <stdio.h>
#include <stdlib.h>
#include <xcb/xcb.h>
#include "basex11.h"

#define X11_ERR(err) x11_err (__func__, err)
#define X11_ERR_ALT(err) (2 - x11_err (__func__, err))

static int x11_err (char const *func, xcb_generic_error_t *err) {
	if (!err) return 0;
	fprintf (stderr, "%s: X11 error %d\n", func, err->error_code);
	free (err);
	return 1;
}

xcb_connection_t *x11_connect (void) {
	return xcb_connect (NULL, NULL);
}

void x11_disconnect (xcb_connection_t *conn) {
	xcb_disconnect (conn);
}

int win_attach (xcb_connection_t *conn, xcb_window_t win) {
	return X11_ERR (xcb_request_check (conn,
		xcb_change_window_attributes_checked (conn, win, XCB_CW_EVENT_MASK,
			(uint32_t [])
				{ XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE }
	)));
}

int win_detach (xcb_connection_t *conn, xcb_window_t win) {
	return X11_ERR (xcb_request_check (conn,
		xcb_change_window_attributes_checked (conn, win, XCB_CW_EVENT_MASK,
			(uint32_t []) { XCB_EVENT_MASK_NO_EVENT }
	)));
}

int x11_get_key (xcb_connection_t *conn, xcb_keycode_t *key, int *press) {
	int ret = 1;
	xcb_generic_event_t *event = xcb_poll_for_event (conn);
	if (event) {
		switch (event->response_type) {
			case XCB_KEY_PRESS:
				*press = 1;
				*key = ((xcb_key_press_event_t *) event)->detail;
				ret = 0;
				break;
			case XCB_KEY_RELEASE:
				*press = 0;
				*key = ((xcb_key_release_event_t *) event)->detail;
				ret = 0;
				break;
			case 0:
				ret = X11_ERR_ALT ((xcb_generic_error_t *) event);
				break;
		}
		free (event);
	} else if (xcb_connection_has_error (conn)) {
		fprintf (stderr, "x11_proc_event: X11 connection error\n");
		ret = 2;
	}
	return ret;
}

// No flushing is done in win_put_*() for now, since it seems to cause weird
// delays of key/button events in PvZ.

int win_put_key (
	xcb_connection_t *conn, xcb_window_t win, xcb_keycode_t key, int press
) {
	xcb_generic_error_t *err;
	xcb_get_geometry_reply_t *reply = xcb_get_geometry_reply
		(conn, xcb_get_geometry (conn, win), &err);
	if (!reply) return X11_ERR_ALT (err);

	xcb_send_event (
		conn, 0, win,
		press ? XCB_EVENT_MASK_KEY_PRESS : XCB_EVENT_MASK_KEY_RELEASE,
		(char *) &(xcb_key_press_event_t) {
			.response_type = press ? XCB_KEY_PRESS : XCB_KEY_RELEASE,
			.detail = key,
			.root = reply->root,
			.event = win,
			.child = win,
			.state = XCB_NONE,
			.same_screen = 1
		}
	);

	free (reply);
	return 0;
}

int win_put_btn (
	xcb_connection_t *conn, xcb_window_t win,
	int16_t x, int16_t y, xcb_button_t btn, int press
) {
	int ret = 0;
	xcb_generic_error_t *err;
	xcb_get_geometry_reply_t *reply1 = xcb_get_geometry_reply
		(conn, xcb_get_geometry (conn, win), &err);
	if (!reply1) { ret = X11_ERR_ALT (err); goto geom_err; }
	xcb_translate_coordinates_reply_t *reply2 = xcb_translate_coordinates_reply (
		conn, xcb_translate_coordinates (conn, win, reply1->root, x, y), &err
	);
	if (!reply2) { ret = X11_ERR_ALT (err); goto coord_err; }

	xcb_send_event (
		conn, 0, win, btn ? (
			press ? XCB_EVENT_MASK_BUTTON_PRESS : XCB_EVENT_MASK_BUTTON_RELEASE
		) : XCB_EVENT_MASK_POINTER_MOTION,
		(char *) &(xcb_button_press_event_t) {
			.response_type = btn ? (
				press ? XCB_BUTTON_PRESS : XCB_BUTTON_RELEASE
			) : XCB_MOTION_NOTIFY,
			.detail = btn,
			.root = reply1->root,
			.event = win,
			.child = win,
			.root_x = reply2->dst_x,
			.root_y = reply2->dst_y,
			.event_x = x,
			.event_y = y,
			.state = XCB_NONE,
			.same_screen = 1
		}
	);

	free (reply2);
	coord_err:
	free (reply1);
	geom_err:
	return ret;
}

// vim:noet:ts=4:sw=4
