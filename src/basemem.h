#include <stdint.h>
#include <sys/types.h>

// Opaque structure to store information about opened processes.
typedef struct mem_cookie mem_cookie;
enum { PROC_READ = 0x01, PROC_WRITE = 0x10 };

// proc_open() requires least one between the PROC_* bits to be set in the mode.
// Both functions return 0 on success, < 0 on failure.  The cookie set by a
// successful proc_open() should be freed with proc_close().
extern int proc_open (mem_cookie **cookie, pid_t pid, int mode);
extern int proc_close (mem_cookie **cookie);
// Returns number of failed bytes, or < 0 on serious failure.
extern int proc_get
	(mem_cookie const *cookie, uint32_t addr, size_t size, void *buf);
// Returns number of failed bytes, or < 0 on serious failure.
extern int proc_put
	(mem_cookie const *cookie, uint32_t addr, size_t size, void const *buf);

// vim:noet:ts=4:sw=4
