#include <stdlib.h>
#include "basemem.h"
#include "memtree.h"

static int _tree_update (
	mem_cookie const *cookie, struct mem_node *tree, int recurse
) {
	int deg, sz, mine, theirs = 0;
	if (tree->deg < 0) {
		deg = 0;
		sz = -tree->deg;
	} else {
		deg = tree->deg;
		sz = sizeof (uint32_t);
	}
	// On zero address, fail directly, skipping proc_get().  tree->addr is never
	// modified in tree_update(), so is not affected.
	if (tree->addr) mine = 0 != proc_get (cookie, tree->addr, sz, tree->val);
	else {
		mine = 1;
		*(uint64_t *) (tree->val) = 0;
	}
	for (int i = 0; i < deg; ++i) {
		struct mem_node *node = tree->ptr + i;
		uint32_t val = *(uint32_t *) (tree->val);
		node->addr = !mine && val ? val + node->offset : 0;
		if (recurse) theirs += _tree_update (cookie, node, recurse);
	}
	return mine + theirs;
}

int tree_update (pid_t pid, struct mem_node *tree, int recurse) {
	int ret;
	mem_cookie *cookie;
	if (proc_open (&cookie, pid, PROC_READ)) return -1;
	ret = _tree_update (cookie, tree, recurse);
	if (proc_close (&cookie)) return -2;
	return ret;
}

// vim:noet:ts=4:sw=4
