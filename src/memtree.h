#include <stdint.h>
#include <sys/types.h>

struct mem_node {
	uint32_t offset;
	uint32_t addr;
	char val[8];  // uint64_t.
	int deg;
	struct mem_node *ptr;
};

// Returns number of failed nodes, or < 0 on serious failure.
extern int tree_update (pid_t pid, struct mem_node *tree, int recurse);
#define node_get(kind, node) (*(kind *) (node->val))

// vim:noet:ts=8:sw=8
