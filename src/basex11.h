#include <stdint.h>
#include <xcb/xproto.h>

// Might return NULL, which should not be used.
extern xcb_connection_t *x11_connect (void);
extern void x11_disconnect (xcb_connection_t *conn);
// Returns 0 on success, 1 on failure.
extern int win_attach (xcb_connection_t *conn, xcb_window_t win);
// Returns 0 on success, 1 on failure.
extern int win_detach (xcb_connection_t *conn, xcb_window_t win);
// Returns 0 on success, 1 if got nothing, otherwise on serious failure.
extern int x11_get_key (xcb_connection_t *conn, xcb_keycode_t *key, int *press);
// Returns 0 on success, otherwise on failure.
extern int win_put_key
	(xcb_connection_t *conn, xcb_window_t win, xcb_keycode_t key, int press);
// Returns 0 on success, otherwise on failure.
extern int win_put_btn (
	xcb_connection_t *conn, xcb_window_t win,
	int16_t x, int16_t y, xcb_button_t btn, int press
);

// vim:noet:ts=4:sw=4
