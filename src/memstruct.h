struct pvz_zombie {
	uint8_t pad0[0x8];	// 0x8.
	int32_t x;		// 0xC.
	int32_t y;		// 0x10.
	uint8_t pad1[0x14];	// 0x24.
	int32_t kind;		// 0x28.
	int32_t state;		// 0x2C.
	uint8_t pad2[0x8];	// 0x34.
	float v;		// 0x38.
	uint8_t pad3[0x19];	// 0x51.
	int8_t chew;		// 0x52.
	uint8_t pad4[0x12];	// 0x64.
	int32_t divert;		// 0x68.
	int32_t eta;		// 0x6C.
	int32_t wave;		// 0x70.
	uint8_t pad5[0x3C];	// 0xAC.
	int32_t slowEta;	// 0xB0.
	int32_t butterEta;	// 0xB4.
	int32_t freezeEta;	// 0xB8.
	int8_t hypno;		// 0xB9.
	uint8_t pad6[0x1];	// 0xBA.
	int8_t dying;		// 0xBB.
	uint8_t pad7[0xD];	// 0xC8.
	int32_t hp0;		// 0xCC.
	uint8_t pad8[0x4];	// 0xD0.
	int32_t hp1;		// 0xD4.
	uint8_t pad9[0x8];	// 0xDC.
	int32_t hp2;		// 0xE0.
	uint8_t pad10[0x7C];	// 0x15C.
	uint32_t addr;
};

struct pvz_plant {
	uint8_t pad0[0x8];	// 0x8.
	int32_t x;		// 0xC.
	int32_t y;		// 0x10.
	uint8_t pad1[0xC];	// 0x1C.
	int32_t row;		// 0x20.
	uint8_t pad2[0x4];	// 0x24.
	int32_t kind;		// 0x28.
	int32_t col;		// 0x2C.
	uint8_t pad3[0x10];	// 0x3C.
	int32_t state;		// 0x40.
	int32_t hp;		// 0x44.
	uint8_t pad4[0x10];	// 0x54.
	int32_t eta;		// 0x58.
	int32_t bulletEta0;	// 0x5C.
	int32_t bulletCd;	// 0x60.
	uint8_t pad5[0x30];	// 0x90.
	int32_t bulletEta1;	// 0x94.
	uint8_t pad6[0xAE];	// 0x142.
	int8_t smash;		// 0x143.
	int8_t sleep;		// 0x144.
	uint8_t pad7[0x8];	// 0x14C.
	uint32_t addr;
};

struct pvz_bullet {
	uint8_t pad0[0x8];	// 0x8.
	int32_t x;		// 0xC.
	int32_t y;		// 0x10.
	uint8_t pad1[0xC];	// 0x1C.
	int32_t row;		// 0x20.
	uint8_t pad2[0x1C];	// 0x3C.
	int32_t v;		// 0x40.
	uint8_t pad3[0x1C];	// 0x5C.
	int32_t kind;		// 0x60.
	uint8_t pad4[0x34];	// 0x94.
	uint32_t addr;
};

struct pvz_pellet {
	uint8_t pad0[0x24];	// 0x24.
	float x;		// 0x28.
	float y;		// 0x2C.
	uint8_t pad1[0x24];	// 0x50.
	int8_t acquired;	// 0x51.
	uint8_t pad2[0x7];	// 0x58.
	int32_t kind;		// 0x5C.
	uint8_t pad3[0xC];	// 0x68.
	int32_t plantKind;	// 0x6C.
	uint8_t pad4[0x6C];	// 0xD8.
	uint32_t addr;
};

struct pvz_mower {
	uint8_t pad0[0x8];	// 0x8.
	float x;		// 0xC.
	uint8_t pad1[0x8];	// 0x14.
	int32_t row;		// 0x18.
	uint8_t pad2[0x14];	// 0x2C.
	int32_t state;		// 0x30.
	uint8_t pad3[0x4];	// 0x34.
	int32_t kind;		// 0x38.
	uint8_t pad4[0x10];	// 0x48.
	uint32_t addr;
};

struct pvz_wtf {
	uint8_t pad0[0x8];	// 0x8.
	int32_t kind;		// 0xC.
	uint8_t pad1[0x4];	// 0x10.
	int32_t col;		// 0x14.
	int32_t row;		// 0x18.
	int32_t wtf;		// 0x1C.
	uint8_t pad2[0x8];	// 0x24.
	float x;		// 0x28.
	float y;		// 0x2C.
	uint8_t pad3[0xC0];	// 0xEC.
	uint32_t addr;
};

struct pvz_slot {
	uint8_t pad0[0x24];	// 0x24.
	int32_t eta;		// 0x28.
	int32_t cd;		// 0x2C.
	int32_t idx;		// 0x30.
	int32_t delta;		// 0x34.
	int32_t kind;		// 0x38.
	int32_t imitKind;	// 0x3C.
	uint8_t pad1[0xC];	// 0x48.
	int8_t avail;		// 0x49.
	int8_t clicked;		// 0x4A.
	uint8_t pad2[0x6];	// 0x50.
	uint32_t addr;
};

struct pvz_ice {
	int32_t edge[0x6];	// 0x18.
	int32_t eta[0x6];	// 0x30.
	uint32_t addr;
};

// vim:noet:ts=8:sw=8
