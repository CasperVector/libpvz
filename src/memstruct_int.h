#define zombie_addr	node_get (uint32_t, mt->zombieAddr)
#define zombie_max	less (node_get (uint32_t, mt->zombieMax), 1024)
#define zombie_cnt	node_get (uint32_t, mt->zombieCnt)
#define zombie_active	!((char *) cur)[0xEC]

#define plant_addr	node_get (uint32_t, mt->plantAddr)
#define plant_max	less (node_get (uint32_t, mt->plantMax), 1024)
#define plant_cnt	node_get (uint32_t, mt->plantCnt)
#define plant_active	!((char *) cur)[0x141]

#define bullet_addr	node_get (uint32_t, mt->bulletAddr)
#define bullet_max	less (node_get (uint32_t, mt->bulletMax), 1024)
#define bullet_cnt	node_get (uint32_t, mt->bulletCnt)
#define bullet_active	!((char *) cur)[0x50]

#define pellet_addr	node_get (uint32_t, mt->pelletAddr)
#define pellet_max	less (node_get (uint32_t, mt->pelletMax), 1024)
#define pellet_cnt	node_get (uint32_t, mt->pelletCnt)
#define pellet_active	!((char *) cur)[0x38]

#define mower_addr	node_get (uint32_t, mt->mowerAddr)
#define mower_max	less (node_get (uint32_t, mt->mowerMax), 6)
#define mower_cnt	node_get (uint32_t, mt->mowerCnt)
#define mower_active	!((char *) cur)[0x30]

#define wtf_addr	node_get (uint32_t, mt->wtfAddr)
#define wtf_max		less (node_get (uint32_t, mt->wtfMax), 1024)
#define wtf_cnt		node_get (uint32_t, mt->wtfCnt)
#define wtf_active	!((char *) cur)[0x20]

#define slot_addr	(node_get (uint32_t, mt->slotAddr) + 0x28)
#define slot_max	less (node_get (uint32_t, mt->slotCnt), 10)

#define ice_addr	(node_get (uint32_t, mt->gameAddr) + 0x60C)
#define ice_max		less (node_get (uint32_t, mt->gameAddr), 1)

// vim:noet:ts=8:sw=8
