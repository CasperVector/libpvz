// pread(), snprintf(), etc with `-std=c99'.
#define _XOPEN_SOURCE 500

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ptrace.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "basemem.h"

#define PATHLEN 32
#define SYS_ERR(ret) (perror (__func__), ret)

struct mem_cookie {
	int mode;
	pid_t pid;
	int fd;
};

int proc_open (struct mem_cookie **cookie, pid_t pid, int mode) {
	int ret, state;
	struct mem_cookie *c;
	if (!(c = malloc (sizeof (struct mem_cookie)))) { ret = -1; goto success; }
	*c = (struct mem_cookie) { .mode = mode, .pid = pid };
	errno = 0;

	ret = ptrace (PTRACE_ATTACH, pid, NULL, NULL);
	if (ret) { ret = SYS_ERR (-2); goto ptrace_err; }
	else do {
		if (waitpid (pid, &state, WUNTRACED) == -1)
			{ ret = SYS_ERR (-2); goto ptrace_err; }
	} while (!WIFSTOPPED (state));

	if (mode & PROC_READ) {
		char path[PATHLEN];
		int rc = snprintf (path, sizeof (path), "/proc/%d/mem", pid);
		if (!(rc > 0 && rc < sizeof (path)))
			{ ret = -3; goto open_err; }
		if ((c->fd = open (path, O_RDONLY)) == -1)
			{ ret = SYS_ERR (-3); goto open_err; }
	}
	*cookie = c;
	goto success;

	open_err:
	ptrace (PTRACE_DETACH, pid, NULL, NULL);
	ptrace_err:
	free (c);
	*cookie = NULL;
	success:
	return ret;
}

int proc_close (struct mem_cookie **cookie) {
	int ret = 0;
	struct mem_cookie *c = *cookie;
	errno = 0;

	if (c->mode & PROC_READ && close (c->fd)) ret = SYS_ERR (-1);
	if (ptrace (PTRACE_DETACH, c->pid, NULL, NULL) && ret != -1)
		{ ret = SYS_ERR (-2); }

	free (c);
	*cookie = NULL;
	return ret;
}

int proc_get (
	struct mem_cookie const *cookie, uint32_t addr, size_t size, void *buf
) {
	errno = 0;
	int ret = pread (cookie->fd, buf, size, addr);
	if (ret < 0) return SYS_ERR (-1);
	else {
		ret = size - ret;
		if (ret) fprintf (stderr, "proc_get: short read\n");
		return ret;
	}
}

int proc_put (
	struct mem_cookie const *cookie, uint32_t addr, size_t size, void const *buf
) {
	ptrdiff_t count = size / sizeof (long);
	size_t remain = size % sizeof (long);
	long tmp;
	errno = 0;

	if (remain) {
		tmp = ptrace (
			PTRACE_PEEKDATA, cookie->pid,
			(long *) (unsigned long) addr + count, NULL
		);
		if (errno) return SYS_ERR (-1);
		memcpy (&tmp, (long *) buf + count, remain);
		++count;
	}

	for (int offset = 0; offset < count + (remain != 0); ++offset) {
		int ret = ptrace (
			PTRACE_POKEDATA, cookie->pid,
			(long *) (unsigned long) addr + offset,
			offset < count ? ((long *) buf)[offset] : tmp
		);
		if (ret) return SYS_ERR (-2);
	}

	return 0;
}

// vim:noet:ts=4:sw=4
