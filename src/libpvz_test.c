#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libpvz_base.h"
#include "libpvz_mem.h"

// Here keycodes are obtained on my machine with xev(1).

int mem_test (char const *arg) {
	struct mem_tree mt;
	pid_t pid;
	int ret = 0, idx;
	size_t len;

	if (sscanf (arg, "%d", &pid) != 1) { ret = 1; goto pre_mt_err; }
	if (tree_init (&mt)) { ret = 2; goto pre_mt_err; };

	printf (
		"# tree_update (pid, mt.root, 1) = %d\n",
		tree_update (pid, mt.root, 1)
	);
	#include "memtree_test.c"
	#include "memstruct_test.c"

	post_mt_err:
	tree_fin (&mt);
	pre_mt_err:
	printf ("\nret = %d\n", ret);
	return ret;
}

int x11_test (char const *arg) {
	xcb_connection_t *conn;
	xcb_window_t win;
	xcb_keycode_t key, future[] = { 41, 30, 28, 30, 27, 26 };  // `future'.
	int ret = 0, press;

	if (sscanf (arg, "0x%x", &win) != 1) { ret = 1; goto conn_err; }
	if (!(conn = x11_connect())) { ret = 2; goto conn_err; };
	if (win_attach (conn, win)) { ret = 2; goto win_err; };

	while (1) {
		ret = x11_get_key (conn, &key, &press);
		if (!ret) {
			printf ("(key, press) = (%u, %d)\n", key, press);
			if (press) switch (key) {
				case 53:  // `x'.
					for (int i = 0; i < sizeof (future); ++i) printf (
						"win_put_key (conn, win, %u, 1) = %d\n",
						future[i], win_put_key (conn, win, future[i], 1)
					);
					break;
				case 24:  // `q'.
					goto success;
			}
		} else if (ret != 1) {
			ret = 3;
			goto success;
		}
	}

	success:
	if (win_detach (conn, win) && ret == 0) ret = 4;
	win_err:
	x11_disconnect (conn);
	conn_err:
	printf ("ret = %d\n", ret);
	return ret;
}

int main (int argc, char const *const *argv) {
	return argc != 2 ? 1 :
		(strncmp (argv[1], "0x", 2) ? mem_test : x11_test) (argv[1]);
}

// vim:noet:ts=4:sw=4
