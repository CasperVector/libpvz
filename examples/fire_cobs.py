#!/usr/bin/python3

import pyvz
import time
import pyvz_utils as u

scene = "pool"
seeds = [
	"lilypad", "sunfl", "plantern", "pumpkin", "blover",
	"squash", "cherry", "doomsh", "icesh", "i:icesh"
]
cobGroups = [
	[(2, 4), (3, 6)], [(3, 4), (2, 6)], [(2, 2), (4, 4)],
	[(3, 2), (1, 4)], [(2, 0), (5, 4)], [(3, 0), (0, 4)]
]

def fire_one_cob(win, row1, col1):
	row2, col2 = 4 if row1 > 2 else 1, 7.5
	coord1 = u.field_to_xcoord(scene, row1, col1)
	coord2 = u.field_to_xcoord(scene, row2, col2)
	for i in range(10):
		win.put_btn(*coord1)
		win.put_btn(*coord2)
		u.free_mouse(win)

def fire_cobs(win, fsm):
	avail = u.first_true([all(
		plant.state == u.stateMap["cob_full"]
		for cell in group for plant in fsm[cell]
		if plant.kind == u.plantMap["cob"]
	) for group in cobGroups])
	if avail != None:
		[fire_one_cob(win, *cell) for cell in cobGroups[avail]]

def main():
	keycodes = u.get_keycodes()
	with pyvz.Window(u.get_win()) as win, pyvz.Memory(u.get_pid()) as mem, \
		pyvz.MemTree(mem) as mt, pyvz.Structs(mt) as ms:
		while True:
			key, press = win.get_key()
			if press:
				if key == keycodes["p"]:
					if ms.plant.update():
						plants = ms.plant.struct()
						fsm = u.field_st_map(plants, u.field_map(plants))
						fire_cobs(win, fsm)
				elif key == keycodes["t"]:
					if ms.pellet.update():
						u.collect_pellets(win, ms.pellet.struct())
				elif key == keycodes["c"]:
					u.choose_seeds(win, seeds)
				elif key == keycodes["u"]:
					mt.root.update()
				elif key == keycodes["q"]:
					break
			time.sleep(0.01)

if __name__ == "__main__":
	main()

# vim:noet:ts=4:sw=4
