#!/usr/bin/python3

import math
import re
import time
from subprocess import Popen, PIPE

# Abstract utilities.

def self_inv(l):
	return dict(l + [(v, k) for k, v in l])

def first_true(ring):
	i, size = 0, len(ring)
	if ring[0]:
		i = size - 1
		while True:
			if i == 0:
				return 0
			elif not ring[i]:
				break
			i -= 1
		i = (i + 1) % size
	else:
		while True:
			if i == size:
				return None
			elif ring[i]:
				break
			i += 1
	return i

# System related utilities.

def get_pid():
	for l in Popen(["pgrep", "PlantsVsZombies"], stdout = PIPE).stdout:
		return int(l)

def get_win():
	for l in Popen(["xwininfo", "-tree", "-root"], stdout = PIPE).stdout:
		l = l.decode("UTF-8")
		m = re.match("^[ \t]*(0x[0-9a-f]+)", l)
		if m and re.search(
			"PlantsVsZombies\.exe", l, re.IGNORECASE
		) and "800x600" in l:
			return int(m.group(1), 16)

def get_keycodes():
	ret = {}
	for l in Popen(["xmodmap", "-pk"], stdout = PIPE).stdout:
		l = l.decode("UTF-8")
		m, names = re.match("^[ \t]*([0-9]+)", l), re.findall("\(([^()]+)\)", l)
		if m:
			code = int(m.group(1))
			[ret.setdefault(name, code) for name in names]
	return ret

# Enum types.

plantMap = self_inv(list(enumerate([
	"pea", "sunfl", "cherry", "wallnut",
	"potato", "snowpea", "chomper", "repeater",
	"puffsh", "sunsh", "fumesh", "grave",
	"hypnosh", "scaresh", "icesh", "doomsh",
	"lilypad", "squash", "threepea", "tangle",
	"jalapeno", "spikew", "torch", "tallnut",
	"seash", "plantern", "cactus", "blover",
	"splitpea", "star", "pumpkin", "magnetsh",
	"cabbage", "pot", "kernel", "coffee",
	"garlic", "umbrella", "marigold", "melon",
	"gatlin", "twinsun", "gloomsh", "cattail",
	"wmelon", "goldmag", "spiker", "cob",
	"imitater", "explode", "gwallnut", "sprout", "rrepeater"
])))

stateMap = self_inv([
	(35, "cob_empty"), (36, "cob_filling"), (37, "cob_full"), (38, "cob_firing")
])

# Coordination related utilities.

def m_to_xcoord(x, y):
	return x + 40, y + 80

def field_to_mcoord(scene, row, col):
	x = 80 * (col + 0.5)
	if scene in ["day", "night"]:
		y = 100 * (row + 0.5)
	elif scene in ["pool", "fog", "roof", "moon"]:
		y = 85 * (row + 0.5)
		if scene in ["roof", "moon"] and col < 3.5:
			y += 20 * math.ceil(3.5 - col)
	return x, y

def field_to_xcoord(scene, row, col):
	return m_to_xcoord(*field_to_mcoord(scene, row, col))

# About the mouse.

def free_mouse(win):
	win.put_btn(0, 0, 3)

def choose_seeds(win, names, delay = 0.1):
	free_mouse(win)
	for name in names:
		copy = name.startswith("i:")
		if copy:
			name = name.replace("i:", "")
		plant = plantMap[name]
		x, y = 53 * (plant % 8 + 0.5), 70 * (plant // 8 + 0.5)

		if copy:
			win.put_btn(488, 550, 0)
			win.put_btn(488, 550)
			time.sleep(delay)
			win.put_btn(x + 190, y + 125)
		else:
			win.put_btn(x + 22, y + 123)
		free_mouse(win)

def collect_pellets(win, pellets):
	free_mouse(win)
	for pellet in pellets:
		if not pellet.acquired:
			win.put_btn(pellet.x + 25, pellet.y + 35)
			free_mouse(win)

# About field cells.

def field_map(structs):
	ret = {}
	for idx, st in enumerate(structs):
		ret.setdefault((st.row, st.col), []).append(idx)
	return ret

def field_st_map(structs, fm):
	return {cell: [structs[idx] for idx in idxx] for cell, idxx in fm.items()}

# vim:noet:ts=4:sw=4
