#!/usr/bin/python3

from setuptools import setup

setup(
	name = "pyvz", version = "0.3.2",
	url = "https://gitea.com/p/CasperVector/libpvz/",
	author = "Casper Ti. Vector",
	setup_requires = ["cffi>=1.0.0"],
	install_requires = ["cffi>=1.0.0"],
	packages = ["pyvz"], ext_package = "pyvz",
	cffi_modules = ["_cffi/build.py:ffi"]
)

# vim:noet:ts=4:sw=4
